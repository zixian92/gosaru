// Package util defines utility functions that can be used
// outside the context of establishing a SSH connection.
package util // import "gitlab.com/zixian92/gosaru/util"

import (
	"errors"
	"io/ioutil"

	"golang.org/x/crypto/ssh"
)

// ErrInvalidPubKeyCert is the error returned when parsing an invalid public key certificate
var ErrInvalidPubKeyCert = errors.New("invalid public key certificate")

// LoadPublicKeyCertificateFile loads the public key certificate from the file at the specified path
// Returns non-nil error on failure
func LoadPublicKeyCertificateFile(certPath string) (*ssh.Certificate, error) {
	certBytes, err := ioutil.ReadFile(certPath)
	if err != nil {
		return nil, err
	}

	pubKey, _, _, _, err := ssh.ParseAuthorizedKey(certBytes)
	if err != nil {
		return nil, err
	}

	cert, ok := pubKey.(*ssh.Certificate)
	if !ok {
		return nil, ErrInvalidPubKeyCert
	}

	return cert, nil
}
