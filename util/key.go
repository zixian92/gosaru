package util

import (
	"io/ioutil"

	"golang.org/x/crypto/ssh"
)

// LoadPrivateKeyFile loads the private key from the file at the specified path
// Returns non-nil error on failure
func LoadPrivateKeyFile(keyPath string) (ssh.Signer, error) {
	keyBytes, err := ioutil.ReadFile(keyPath)
	if err != nil {
		return nil, err
	}
	return ssh.ParsePrivateKey(keyBytes)
}

// LoadPublicKeyFile loads the public key from the file at the specified path
// Returns non-nil error on failure
func LoadPublicKeyFile(keyPath string) (ssh.PublicKey, error) {
	keyBytes, err := ioutil.ReadFile(keyPath)
	if err != nil {
		return nil, err
	}

	pubKey, _, _, _, err := ssh.ParseAuthorizedKey(keyBytes)
	return pubKey, err
}
