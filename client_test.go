package gosaru

import (
	"bytes"
	"fmt"
	"log"
	"net"
	"os"
	"testing"

	"gitlab.com/zixian92/gosaru/util"
	"golang.org/x/crypto/ssh"
)

var sshServerConfig = &ssh.ServerConfig{
	PasswordCallback: func(c ssh.ConnMetadata, pass []byte) (*ssh.Permissions, error) {
		if c.User() == "root" && string(pass) != "password" {
			return nil, fmt.Errorf("fail password auth")
		}
		return nil, nil
	},
}
var sshServer net.Listener
var serverTermChan chan bool
var serverStatusChan chan error
var certChecker *ssh.CertChecker

func initCertChecker() error {
	userCAKey, err := util.LoadPrivateKeyFile("sshkeys/user_ca")
	if err != nil {
		return err
	}
	certChecker = &ssh.CertChecker{
		IsUserAuthority: func(pubKey ssh.PublicKey) bool {
			return bytes.Equal(userCAKey.PublicKey().Marshal(), pubKey.Marshal())
		},
		IsHostAuthority: func(hostKey ssh.PublicKey, addr string) bool {
			return false
		},
	}
	return nil
}

// Starts and runs test SSH server in another goroutine
func initSSHServer() error {
	serverTermChan = make(chan bool, 1)
	serverStatusChan = make(chan error, 1)

	// Set up user and host certificate checker
	if err := initCertChecker(); err != nil {
		return err
	}

	sshServerConfig.PublicKeyCallback = func(c ssh.ConnMetadata, pubKey ssh.PublicKey) (*ssh.Permissions, error) {
		// Use public key certificate authentication for root user if not using password auth
		if c.User() == "root" {
			userCert, ok := pubKey.(*ssh.Certificate)
			if !ok {
				return nil, fmt.Errorf("Invalid public key certificate")
			}
			return nil, certChecker.CheckCert("admin", userCert)
		}

		// Mock an authorized keys file for other users
		authorizedKey, err := util.LoadPublicKeyFile("sshkeys/id_rsa.pub")
		if err != nil || !bytes.Equal(authorizedKey.Marshal(), pubKey.Marshal()) {
			return nil, fmt.Errorf("Not an authorized key")
		}

		return nil, nil
	}

	// Load in SSH server host key
	hostKeyCert, err := util.LoadPublicKeyCertificateFile("sshkeys/ssh_host-cert.pub")
	if err != nil {
		return err
	}
	hostKey, err := util.LoadPrivateKeyFile("sshkeys/ssh_host")
	if err != nil {
		return err
	}
	hostKeyCertSigner, err := ssh.NewCertSigner(hostKeyCert, hostKey)
	if err != nil {
		return err
	}
	sshServerConfig.AddHostKey(hostKeyCertSigner)

	listener, err := net.Listen("tcp", "0.0.0.0:7777")
	if err != nil {
		return err
	}
	sshServer = listener

	go runServerRoutine(sshServer, serverTermChan, serverStatusChan)

	return nil
}

func runServerRoutine(svr net.Listener, termChan <-chan bool, statusChan chan<- error) {
	newConnChan := make(chan net.Conn)
	defer close(newConnChan)
	acceptNewConn := func() {
		newConn, err := svr.Accept()
		if err != nil {
			log.Printf("Failed to accept new connection: %v", err)
			newConnChan <- nil
		} else {
			newConnChan <- newConn
		}
	}

	go acceptNewConn()

	for {
		select {
		case newConn := <-newConnChan:
			if newConn == nil {
				statusChan <- fmt.Errorf("fail to accept new SSH connection")
				return
			}

			// Establish SSH connection handshake
			// Authentication is done here
			_, channels, requests, err := ssh.NewServerConn(newConn, sshServerConfig)
			if err != nil {
				log.Printf("failed to establish SSH connection handshake: %v", err)
				go acceptNewConn()
				continue
			}

			// Discards ALL out-of-band requests
			go ssh.DiscardRequests(requests)
			go handleChannels(channels)
			go acceptNewConn()

		// Done with testing: close the server
		case <-termChan:
			statusChan <- svr.Close()
			return
		}
	}
}

func handleChannels(channels <-chan ssh.NewChannel) {
	for newChannel := range channels {
		go handleChannel(newChannel)
	}
}

func handleChannel(channel ssh.NewChannel) {
	if channel.ChannelType() != "session" {
		channel.Reject(ssh.UnknownChannelType, "only handle session channels")
		return
	}

	ch, reqs, err := channel.Accept()
	if err != nil {
		log.Printf("Fail to accept new channel: %v", err)
		return
	}
	defer ch.Close()

	// Handle only shell requests
	go func(in <-chan *ssh.Request) {
		for req := range in {
			req.Reply(req.Type == "shell", nil)
		}
	}(reqs)

	// Respond something
	ch.Write([]byte("0"))
}

func cleanUpSSHServer() {
	close(serverTermChan)
	close(serverStatusChan)
}
func TestMain(m *testing.M) {
	// Set up SSH server to test client
	if err := initSSHServer(); err != nil {
		log.Println(err)
		os.Exit(1)
	}

	// Run ALL tests(in this file only?)
	code := m.Run()

	// End of tests, tell SSH server to stop
	serverTermChan <- true

	// Any last clean-ups
	cleanUpSSHServer()

	// Exit
	os.Exit(code)
}

func TestClient_CorrectPassword(t *testing.T) {
	c, err := NewClient(ClientConfig{
		ConnType:    ConnRemote,
		RemoteHost:  "localhost",
		RemotePort:  7777,
		RemoteUser:  "root",
		AuthMethods: AuthPassword,
		Password:    "password",
	})
	if err != nil {
		t.Errorf("Fail to create password auth client: %v", err)
		return
	}
	defer c.Close()
	s, err := c.NewSession()
	if err != nil {
		t.Errorf("Fail to open SSH session: %v", err)
		return
	}
	defer s.Close()
}

func TestClient_IncorrectPassword(t *testing.T) {
	c, err := NewClient(ClientConfig{
		ConnType:    ConnRemote,
		RemoteHost:  "localhost",
		RemotePort:  7777,
		RemoteUser:  "root",
		AuthMethods: AuthPassword,
		Password:    "password2",
	})
	if err != nil {
		t.Errorf("Fail to create password auth client: %v", err)
		return
	}
	defer c.Close()

	_, err = c.NewSession()
	if err == nil {
		t.Error("Wrong password should throw an error")
	}
}

func TestClient_CorrectAuthKey(t *testing.T) {
	c, err := NewClient(ClientConfig{
		ConnType:     ConnRemote,
		RemoteUser:   "testuser",
		RemoteHost:   "localhost",
		RemotePort:   7777,
		AuthMethods:  AuthPubKey,
		IdentityFile: "sshkeys/id_rsa",
	})
	if err != nil {
		t.Errorf("Fail to create pubkey client: %v", err)
		return
	}
	defer c.Close()

	s, err := c.NewSession()
	if err != nil {
		t.Errorf("Fail to open SSH session using pubkey auth: %v", err)
		return
	}
	defer s.Close()
}

func TestClient_WrongAuthKey(t *testing.T) {
	c, err := NewClient(ClientConfig{
		ConnType:     ConnRemote,
		RemoteUser:   "testuser",
		RemoteHost:   "localhost",
		RemotePort:   7777,
		AuthMethods:  AuthPubKey,
		IdentityFile: "sshkeys/ssh_host",
	})
	if err != nil {
		t.Errorf("Fail to create pubkey client: %v", err)
		return
	}
	defer c.Close()

	_, err = c.NewSession()
	if err == nil {
		t.Error("Wrong SSH key should fail")
	}
}

func TestClient_ValidAuthKeyCert(t *testing.T) {
	c, err := NewClient(ClientConfig{
		ConnType:        ConnRemote,
		RemoteUser:      "root",
		RemoteHost:      "localhost",
		RemotePort:      7777,
		AuthMethods:     AuthPubKeyCert,
		IdentityFile:    "sshkeys/id_rsa",
		CertificateFile: "sshkeys/id_rsa-cert.pub",
	})
	if err != nil {
		t.Errorf("Fail to create pubkeycert client: %v", err)
		return
	}
	defer c.Close()

	s, err := c.NewSession()
	if err != nil {
		t.Errorf("Fail to open SSH session using pubkeycert auth: %v", err)
		return
	}
	defer s.Close()
}

func TestClient_InvalidAuthKeyCert(t *testing.T) {
	c, err := NewClient(ClientConfig{
		ConnType:        ConnRemote,
		RemoteUser:      "root",
		RemoteHost:      "localhost",
		RemotePort:      7777,
		AuthMethods:     AuthPubKeyCert,
		IdentityFile:    "sshkeys/id_rsa",
		CertificateFile: "sshkeys/id_rsa-cert.pub2",
	})
	if err != nil {
		t.Errorf("Fail to create pubkeycert client: %v", err)
		return
	}
	defer c.Close()

	_, err = c.NewSession()
	if err == nil {
		t.Error("Wrong cert should fail")
	}
}

func TestClient_ValidHostKey(t *testing.T) {
	c, err := NewClient(ClientConfig{
		ConnType:        ConnRemote,
		RemoteUser:      "root",
		RemoteHost:      "localhost",
		RemotePort:      7777,
		AuthMethods:     AuthPubKeyCert,
		IdentityFile:    "sshkeys/id_rsa",
		CertificateFile: "sshkeys/id_rsa-cert.pub",
		HostKeyPolicy:   HostKeyKnownHostsFiles,
		KnownHostsFiles: []string{"sshkeys/known_hosts"},
	})
	if err != nil {
		t.Errorf("Fail to create pubkeycert client: %v", err)
		return
	}
	defer c.Close()

	s, err := c.NewSession()
	if err != nil {
		t.Errorf("Fail to open SSH session using pubkeycert auth: %v", err)
		return
	}
	defer s.Close()
}
