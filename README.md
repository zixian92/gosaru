# gosaru

Simple Golang library for connecting to SSH servers

## Features

* Password authentication
* Public key authentication
* Public key certificate authentication
* Verify remote host's key using known hosts file
* Utilities to read in private key, public key, and public key certificate from file

## Important Notes

### What gosaru does

* Connect to remote host via SSH using given configuration
* Expose simplified utilities to deal with various aspects of establishing SSH connection

### What gosaru does not do

* Manage things(eg. sessions) running on top of established SSH connection

## Usage

```go
package main

import "gitlab.com/zixian92/gosaru"

// Simple SSH client using password authentication and a knwon hosts file for host verification.
func main() {
    // Creates a new SSH client which can be used to do stuff on the configured
    // remote host any number of times.
    sshClient, err := gosaru.NewClient(gosaru.ClientConfig{
        ConnType: gosaru.ConnRemote,
        RemoteHost: "my-ssh-server",
        RemoteUser: "root",
        AuthMethods: gosaru.AuthPassword,
        Password: "password",
        HostKeyPolicy: gosaru.HostKeyKnownHostsFile,
        KnownHostsFile: []string{"/home/testuser/.ssh/known_hosts"},
    })
    if err != nil {
        // Handle config errors here.
    }
    defer sshClient.Close()
    
    // Connects to the remote host(if not yet connected) and opens a SSH session.
    sshSession, err := sshClient.NewSession()
    if err != nil {
        // Handle connection and other errors here.
    }
    defer sshSession.Close()
    
    // Refer to https://godoc.org/golang.org/x/crypto/ssh#Session on how to use session.
    // Do something with the session...
}
```

## Developer Guide

### Initial Setup

After checking out this project, make sure to install external dependency packages.

```
go get -v .
```

### Testing

1. Generate the files required for unit testing to work.
    
    ```
    ./gentestkeys.sh
    ```
1. Run the tests.
    
    ```
    go test
    ```
    * If planning to use Dockerized method, make sure the containers exist first.
    
        ```
        docker-compose up -d
        ```
    * Then run the Go commands in the `build` container.
    
        ```
        docker-compose run --rm build go test
        ```