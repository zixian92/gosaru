package gosaru

// AuthFlags defines the SSH client authentication value type.
type AuthFlags int8

const (
	// AuthPassword is the value to be used for password authentication.
	AuthPassword AuthFlags = AuthFlags(1 << iota)

	// AuthPubKey is the value to use for public key authentication.
	AuthPubKey

	// AuthPubKeyCert is the value to use for public key certificate authentication with principals.
	// Refer here https://blog.habets.se/2011/07/OpenSSH-certificates.html.
	AuthPubKeyCert
)

// ConnType defines the type of SSH connection the client should establish.
type ConnType string

const (
	// ConnUnix is the value to use for connecting to Unix socket.
	ConnUnix ConnType = "unix"

	// ConnRemote is the value to use for connecting to remote host.
	ConnRemote ConnType = "tcp"
)

// HostKeyPolicy defines the values to be passed to SSH client
// configuration regarding host key verification policy.
type HostKeyPolicy int8

const (
	// HostKeyIgnore is the value to use to ignore host key verification.
	HostKeyIgnore HostKeyPolicy = iota

	// HostKeyKnownHostsFiles is the value to use to verify host key
	// using 1 or more known hosts file(s).
	HostKeyKnownHostsFiles
)
