package gosaru

import (
	"testing"
)

func Test_isUsePassword(t *testing.T) {
	testArgs := []AuthFlags{0, 1, 2, 3, 4, 5, 6, 7}
	expected := []bool{false, true, false, true, false, true, false, true}

	for i, arg := range testArgs {
		if res := isUsePassword(arg); res != expected[i] {
			t.Errorf("isUsePassword(%d) = %v, want %v", arg, res, expected[i])
		}
	}
}

func Test_isUsePubKey(t *testing.T) {
	testArgs := []AuthFlags{0, 1, 2, 3, 4, 5, 6, 7}
	expected := []bool{false, false, true, true, false, false, true, true}

	for i, arg := range testArgs {
		if res := isUsePubKey(arg); res != expected[i] {
			t.Errorf("isUsePubKey(%d) = %v, want %v", arg, res, expected[i])
		}
	}
}

func Test_isUsePubKeyCert(t *testing.T) {
	testArgs := []AuthFlags{0, 1, 2, 3, 4, 5, 6, 7}
	expected := []bool{false, false, false, false, true, true, true, true}

	for i, arg := range testArgs {
		if res := isUsePubKeyCert(arg); res != expected[i] {
			t.Errorf("isUsePubKeyCert(%d) = %v, want %v", arg, res, expected[i])
		}
	}
}
