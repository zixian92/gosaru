#!/bin/bash

KEYDIR="sshkeys"

# Remake sshkeys folder for a clean start
if [ -d ${KEYDIR} ]; then
    rm -rf ${KEYDIR}
fi
mkdir -p ${KEYDIR}

# Generate the CA keys
ssh-keygen -t rsa -b 2048 -f ${KEYDIR}/user_ca -q -N ""
ssh-keygen -t rsa -b 2048 -f ${KEYDIR}/host_ca -q -N ""

# Generate the SSH client key
ssh-keygen -t rsa -b 2048 -f ${KEYDIR}/id_rsa -q -N ""

# Generate the SSH host key
ssh-keygen -t rsa -b 2048 -f ${KEYDIR}/ssh_host -q -N ""

# Sign the SSH client public key
ssh-keygen -s ${KEYDIR}/user_ca -I rootuser -n admin -V +52w ${KEYDIR}/id_rsa.pub
cp ${KEYDIR}/id_rsa.pub ./
ssh-keygen -s ${KEYDIR}/user_ca -I expiredrootuser -n user -V +52w id_rsa.pub
mv id_rsa-cert.pub ${KEYDIR}/id_rsa-cert.pub2
rm id_rsa.pub

# Sign the SSH host public key
ssh-keygen -s ${KEYDIR}/host_ca -I localhost -h -V +52w ${KEYDIR}/ssh_host.pub

# Generate known_hosts file
echo "@cert-authority localhost:7777 $(cat ${KEYDIR}/host_ca.pub)" >> ${KEYDIR}/known_hosts