package gosaru

import (
	"time"

	"gitlab.com/zixian92/gosaru/util"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/knownhosts"
)

// ClientConfig defines the SSH client configuration, which determines
// how the SSH connection will be established and other behabiours of the
// SSH client
type ClientConfig struct {
	ConnType ConnType

	// RemoteHost is the connection target.
	// For Unix socket connection, provide the path to the socket.
	RemoteHost string

	// RemotePort is only used when ConnType is CONN_REMOTE.
	RemotePort int16

	// RemoteUser specifies the user to log in as on the remote
	// If not given, defaults to the current user running this client program
	RemoteUser string

	// AuthMethods specifies how the client will authenticate with the remote
	AuthMethods AuthFlags

	// Password is the password to use for password authentication
	Password string

	// IdentityFile is the path to the SSH private key to use for public key
	// and public key certificate authentication
	IdentityFile string

	// CertificateFile is the path to the SSH public key certificate to use for
	// public key certificate authentication
	CertificateFile string

	// DisconnectRetries specifies how many times the client will attempt
	// to reconnect should it encounter a broken connection
	DisconnectRetries int

	// ReconnectInterval specifies the amount of time to wait
	// between each reconnection
	ReconnectInterval time.Duration

	// HostKeyPolicy determines how the remote host's host key will be handled
	// when verifying the remote host
	HostKeyPolicy HostKeyPolicy

	// List of known hosts files to verify remote host key
	KnownHostsFiles []string

	// LogFile specifies where the client should write error logs to
	// Setting to nil disables logging
	// LogFile *os.File

	// ConnectionTimeout indicates how long to wait before considering connection
	// attempt as a timeout.
	// A duration of 0 means no timeout.
	ConnectionTimeout time.Duration
}

func isUsePassword(authFlags AuthFlags) bool {
	return (authFlags & AuthPassword) != 0
}

func isUsePubKey(authFlags AuthFlags) bool {
	return (authFlags & AuthPubKey) != 0
}

func isUsePubKeyCert(authFlags AuthFlags) bool {
	return (authFlags & AuthPubKeyCert) != 0
}

// ParseClientConfig parses the given client wrapper config into the underlying SSH
// library's client configuration.
// Returns non-nil error onfailure.
func ParseClientConfig(cfg ClientConfig) (*ssh.ClientConfig, error) {
	sshConfig := &ssh.ClientConfig{
		User:    cfg.RemoteUser,
		Auth:    []ssh.AuthMethod{},
		Timeout: cfg.ConnectionTimeout,
	}

	// Set host key callback
	if cfg.HostKeyPolicy == HostKeyIgnore {
		sshConfig.HostKeyCallback = ssh.InsecureIgnoreHostKey()
	} else if cfg.HostKeyPolicy == HostKeyKnownHostsFiles {
		if cfg.KnownHostsFiles == nil {
			cfg.KnownHostsFiles = []string{}
		}
		cb, err := knownhosts.New(cfg.KnownHostsFiles...)
		if err != nil {
			return nil, err
		}
		sshConfig.HostKeyCallback = cb
	}

	// Prepare AuthMethod
	if isUsePassword(cfg.AuthMethods) {
		sshConfig.Auth = append(sshConfig.Auth, ssh.Password(cfg.Password))
	}

	shouldLoadPrivateKey := isUsePubKey(cfg.AuthMethods) || isUsePubKeyCert(cfg.AuthMethods)
	shouldLoadCertificate := isUsePubKeyCert(cfg.AuthMethods)

	// Already implies use pubkey or pubkeycert
	if shouldLoadPrivateKey {
		privateKey, err := util.LoadPrivateKeyFile(cfg.IdentityFile)
		if err != nil {
			return nil, err
		}

		var signer ssh.Signer
		if shouldLoadCertificate {
			cert, err := util.LoadPublicKeyCertificateFile(cfg.CertificateFile)
			if err != nil {
				return nil, err
			}

			// Generate certifictae-based signer
			// First, check that privateKey matches that in the certificate
			certSigner, err := ssh.NewCertSigner(cert, privateKey)
			if err != nil {
				return nil, err
			}
			signer = certSigner
		} else {
			signer = privateKey
		}
		sshConfig.Auth = append(sshConfig.Auth, ssh.PublicKeys(signer))
	}

	return sshConfig, nil
}
