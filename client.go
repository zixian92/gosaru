// Package gosaru defines a wrapper SSH client that simplifies the
// process of establishing SSH connection by abstracting away most
// of the function calls on the underlying Golang SSH library.
package gosaru

import (
	"fmt"

	"golang.org/x/crypto/ssh"
)

// Client defines te SSH client wrapper
type Client struct {
	realClient *ssh.Client
	realConfig *ssh.ClientConfig
	config     ClientConfig
}

// NewClient creates a new SSH connection client
// Returns non-nil error if unable to process the given config
func NewClient(config ClientConfig) (*Client, error) {
	realConfig, err := ParseClientConfig(config)
	if err != nil {
		return nil, err
	}

	return &Client{
		realConfig: realConfig,
		config:     config,
	}, nil
}

// NewSession opens a new SSH session
// If connection fails, the client will attempt to reconnect
// for the number of times it is given via the config object
func (c *Client) NewSession() (*ssh.Session, error) {
	if c.realClient != nil {
		s, err := c.realClient.NewSession()
		if err != nil {
			c.Close()
			if err := c.establishConnection(); err != nil {
				return nil, err
			}
			return c.NewSession()
		}

		return s, nil
	}

	// If connection has not been established,
	// attempt to open a connection.
	if err := c.establishConnection(); err != nil {
		return nil, err
	}
	return c.realClient.NewSession()
}

// Close closes the client connection
// Returns non-nil error on failure
func (c *Client) Close() error {
	if c.realClient == nil {
		return nil
	}
	err := c.realClient.Close()
	c.realClient = nil
	return err
}

func (c *Client) establishConnection() error {
	addrStr := c.config.RemoteHost
	if c.config.RemotePort <= 0 {
		c.config.RemotePort = 22
	}
	if c.config.ConnType == ConnRemote {
		addrStr = fmt.Sprintf("%s:%d", addrStr, c.config.RemotePort)
	}

	for i := c.config.DisconnectRetries; i >= 0; i-- {
		realClient, err := ssh.Dial(string(c.config.ConnType), addrStr, c.realConfig)
		if err == nil {
			c.realClient = realClient
			break
		}

		if err != nil && i == 0 {
			return err
		}
	}
	return nil
}
